package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private ListaHash<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this(500000, 2000000);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.capacidad = capacidad;
		tabla = (ListaHash<K, V>[])new ListaHash[capacidad];
		for (int i = 0; i < capacidad; i++)
			tabla[i] = new ListaHash<K,V>();
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		tabla[hash(llave)].put(llave, valor);
	}

	private void resize(int i) {
		// TODO Auto-generated method stub
		
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		return tabla[hash(llave)].get(llave);
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V rta =tabla[hash(llave)].get(llave);
		tabla[hash(llave)].setValor(null);
		return rta;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		
		return (llave.hashCode() & 0x7ffffff) % capacidad;
	}
	
		//TODO: Permita que la tabla sea dinamica

}