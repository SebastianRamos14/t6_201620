package taller.estructuras;

public class ListaHash<K,V> {

	private NodoHash<K, V> primero;

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		for(NodoHash< K, V> x = primero; x!= null; x = x.getSiguiente())
			if(llave.equals(x.getLlave()))
			{x.setValor(valor); return;}
		primero = new NodoHash<K, V>(llave, valor, primero);
	}



	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		for(NodoHash<K, V> x = primero; x!= null; x = x.getSiguiente())
			if(llave.equals(x.getLlave()))
			return x.getValor();
		return null;
	}
}
