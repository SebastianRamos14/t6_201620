package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import taller.estructuras.TablaHash;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	
	private static TablaHash<Integer, String> urgencias = new TablaHash<>();
	private static ArrayList<String[]> directorio;

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			directorio = new ArrayList<String[]>();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				directorio.add(datos);
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Ingrese el nombre");
					String nombre = br.readLine();
					System.out.println("Ingrese el numero del acudiente");
					int numero = Integer.parseInt(br.readLine());
					if(buscarNombre(nombre) !=null)
					{
					urgencias.put(numero,nombre);
					System.out.println("Se agrego el ciudadano");
					}
					else
					System.out.println("No se encuentra el ciudadano");
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

	private static String[] buscarNombre(String pNombre)
	{
		String[] rta = null;
		for (int i = 0; i < directorio.size(); i++) {
			if(directorio.get(i)[0].equals(pNombre))
				rta = directorio.get(i);
		}
		return rta;
	}
}
